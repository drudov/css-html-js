var ExtractTextPLugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './app/index.js',
    output: {
        path: './asset',
        filename: 'bundle.js',
     //   publicPath: '/assets/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /(node_modules|bower_components)/,
            },
            { test: /\.svg$/, loader: 'url?limit=20000&name=fonts/[name].[ext]' },
            { test: /\.woff$/, loader: 'url?limit=20000&name=fonts/[name].[ext]' },
            { test: /\.woff2$/, loader: 'url?limit=20000&name=fonts/[name].[ext]' },
            { test: /\.[ot]tf$/, loader: 'url?limit=20000&name=fonts/[name].[ext]' },
            { test: /\.eot$/, loader: 'url?limit=20000&name=fonts/[name].[ext]' },
            {
                test: /\.scss$/,
                loader: ExtractTextPLugin.extract('style',['css?sourceMap', 'sass?sourceMap'])
            },
            {
                test: /\.html$/,
                loader: 'raw'
            }

        ]
    },
    plugins:[
        new ExtractTextPLugin('style.css'),
        new HtmlWebpackPlugin()
    ]
};